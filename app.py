import os
import json
import config
import sqlite3
import jdatetime
import requests
import flask_login
from config import Config
from math import ceil , pi
from flask_sqlalchemy import  SQLAlchemy
from flask_login import LoginManager, UserMixin,login_required, login_user, logout_user 
from flask import Flask, url_for, redirect, request,render_template,flash,session,jsonify,make_response,g, abort,Response

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
app.config.from_object(Config)
db = SQLAlchemy(app)

# flask-login
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

# silly user model
class User(UserMixin):
    def __init__(self, id):
        self.id = id
    def __repr__(self):
        return "%d" % (self.id)

user = User(0)
def create_db():
    return sqlite3.connect(".//cataloge.db")

def place_value(num):
    return("{:,}".format(num))

def split(txt, seps):
    default_sep = seps[0]
    # we skip seps[0] because that's the default separator
    for sep in seps[1:]:
        txt = txt.replace(sep, default_sep)
    return [i.strip() for i in txt.split(default_sep)]

def today1():
    shanb1 = jdatetime.datetime.now().strftime("%A")
    shanb = switch_day(shanb1)
    mounth = jdatetime.datetime.now().strftime("%B")
    month = switch_month(mounth)
    day = jdatetime.datetime.now().strftime("%d")
    year = jdatetime.datetime.now().strftime("%Y")
    saat = jdatetime.datetime.now().strftime("%H:%M")
    today_date = "📆"+shanb+" "+day+" "+month+" "+year
    return shanb,month,day,year,saat
def switch_month(argument):
    switcher = {
        "Farvardin": "فروردین",
        "Ordibehesht": "اردیبهشت",
        "Khordad": "خرداد",
        "Tir": "تیر",
        "Mordad": "مرداد",
        "Shahrivar": "شهریور",
        "Mehr": "مهر",
        "Aban": "آبان",
        "Azar": "آذر",
        "Dey": "دی",
        "Bahman": "بهمن",
        "Esfand": "اسفند",

    }
    return switcher.get(argument, "Invalid month")


def switch_day(argument):
    switcher = {
        "Sunday": "یکشنبه",
        "Monday": "دوشنبه",
        "Tuesday": "سه شنبه",
        "Wednesday": "چهارشنبه",
        "Thursday": "پنج شنبه",
        "Friday": "جمعه",
        "Saturday": "شنبه",

    }
    return switcher.get(argument, "Invalid day")

# infolist power(yes,no) | oil(yes, no) | pipe (yes, no) | handpomp(yes,no)
# infolist jack(yes , no)| rapcher (yes,no)
# infolist carsling(yes,no) | boxel(yes,no) | if boxel=yes how much |
# infolist systemtype? v? w1? w2? l?
# infotlist(s,l,w1,w2,v,power,oil,pipe,handpomp, jack , rapcher, carsling ,boxel , boxelmetr)
def calprice (s,l,w1,w2,v,power,oil,pipe,handpomp, jack , rapcher, carsling ,tablo , sode):
    rap = 0
    carslingprice =0
    pricepower = 0
    pricejack = 0
    pricegarmkon= 0
    priceojrat= 0
    pricebox= 0
    pricepomp= 0
    pricevalve= 0
    pricemotor= 0
    sys =s
    ve = v
    we1 =w1
    we2 =w2
    le = l
    js = calc(sys,ve, we1 ,we2, le)
    print("++++++++++++++")
    print(js)
    if js[1] == 200 :
        pomp = js[0]['pomp']
        print(pomp)
        motor = js[0]['motor']
        shaft =js[0]['shaftmodel']
        shaftLL =js[0]['shafttool']
        velocity = js[0]['v']
        pricetotal = 0
        print("++++++رررررررررررر++++++++")
        if jack   == 'on' :
            pricejack = 0
            d = int(split(shaft, "-")[1])
            if d >= 90:
                x = 0.2
            else:
                x = 0.15
            #systemtype calculate
            if s == "d":
                cylindertol = l+0.3+x
                shafttol = cylindertol+0.05+1
            elif s == "i":
                cylindertol = (l/2)+0.2+x
                shafttol = cylindertol+0.05+1
            else :
                return {},400

            if cylindertol <= 3:
                cylindertol = 0.5
                oji = "-1"

            elif cylindertol > 3 and cylindertol <= 6 :
                cylindertol = 1
                oji = "-2"

            elif cylindertol > 6 and cylindertol <= 9 :
                cylindertol = 1.5
                oji = "-3"

            elif cylindertol > 9 and cylindertol <= 12 :
                cylindertol = 2
                oji = "-3"

            
            #felo
            if shaft == "50-60":
                oj = "50-60"
                man = '3"-89'
            elif shaft == "60-70":
                oj = "70-80"
                man = '3.5"-102'
            elif shaft == "70-80":
                man = '4"-114'
                oj = "70-80"
            elif shaft == "80-90" or shaft == "90-100" or shaft == "85-100":
                man = '5"-141'
                oj = "90-100"
            elif shaft == "100-110" or shaft == "100-115" or shaft == "110-125":
                man = '6"-168'
                oj = "110-125"
            else :
                man = '8"-219'
                oj = "110-125"
            oj = oj +oji
            print("++++++ظظظظظظظظظ++++++++")
            # Price shaft
            g.cur.execute('SELECT * FROM price WHERE material == ? and type == ?',(shaft, 'shaft'),)
            for row in g.cur.fetchall():
                priceshaft =row[1]
                print(priceshaft)
                break
            shafthazine=shafttol * priceshaft
            pricejack = pricejack + shafthazine
            print("+++++طططططططططط+++++")
            # Price Packing
            g.cur.execute('SELECT * FROM price WHERE material == ? and type == ?',(shaft, 'packing'),)
            print("++++++++++++++")
            print(shaft)
            for row in g.cur.fetchall():
                print("+++777777777++")
                pricepacking =row[1]
                print(pricepacking)
                break
            pricejack = pricejack + pricepacking
            print("++++++++++++++")
            # Price manisman
            g.cur.execute('SELECT * FROM price WHERE material == ? and type == ?',(man, 'manisman'),)
            for row in g.cur.fetchall():
                pricemanisman =row[1]
                break
            manismanhaz = cylindertol * pricemanisman
            pricejack = pricejack + manismanhaz

            # Price ahanalat
            g.cur.execute('SELECT * FROM price WHERE material == ? and type == ?',(s, 'ahanalat'),)
            for row in g.cur.fetchall():
                priceahanalat =row[1]
                break
            pricejack = pricejack + priceahanalat

            # Price ojrat
            g.cur.execute('SELECT * FROM price WHERE material == ? and type == ?',(oj, 'oj-jack'),)
            for row in g.cur.fetchall():
                ojratjack =row[1]
                break
            pricejack = pricejack + ojratjack
        else :
            pricejack = 0
        #-----Rapcher
        print("++++++++++++++")
        if rapcher   == 'on' : 
            if pomp <= 100 :
                rap = 'r0.75'
                g.cur.execute('SELECT * FROM price WHERE material == ? and type == ?',(rap, 'rapcher'),)
                for row in g.cur.fetchall():
                    pricerapcher =row[1]
                    pricejack = pricejack + pricerapcher
                    break
            else : 
                rap = 'r1.5'
                g.cur.execute('SELECT * FROM price WHERE material == ? and type == ?',(rap, 'rapcher'),)
                for row in g.cur.fetchall():
                    pricerapcher =row[1]
                    pricejack = pricejack + pricerapcher
                    break
        else:
            pricerapcher = 0

        ####----------------POWER----UNIT-------------------
        pricepomp =0
        if power == 'on' :
            #####motor price
            mot = "se"+ str(motor)
            print(mot)
            g.cur.execute('SELECT * FROM price WHERE material == ? and type == ?',(mot, 'motor'),)
            for row in g.cur.fetchall():
                pricemotor =row[1]
            print(pricemotor)
            #valve price
            if int(pomp)  <= 100:
                val = "ev1003.4"
                boxtype = "elevator"
                ojtype = "elevator"
            else:
                val = "ev1001.5"
                boxtype = "automobil"
                ojtype = "automobil"
            print(val)
            g.cur.execute('SELECT * FROM price WHERE material == ? and type == ?',(val, 'valve'),)
            for row in g.cur.fetchall():
                pricevalve =row[1]
            print(pricevalve)
            #pomp-price
            pompx = 's'+str(int(pomp))
            print(pomp)
            print(pompx)
            g.cur.execute('SELECT * FROM price WHERE material == ? and type == ?',(pompx, 'pomp'),)
            for row in g.cur.fetchall():
                pricepomp =row[1]
            #box price
            g.cur.execute('SELECT * FROM price WHERE material == ? and type == ?',(boxtype, 'box'),)
            for row in g.cur.fetchall():
                pricebox =row[1]
            #ojrat price
            g.cur.execute('SELECT * FROM price WHERE material == ? and type == ?',(ojtype, 'oj-power'),)
            for row in g.cur.fetchall():
                priceojrat =row[1]
            #garmkon price
            g.cur.execute('SELECT * FROM price WHERE material == ? and type == ?',('garmkon', 'tajhizat'),)
            for row in g.cur.fetchall():
                pricegarmkon =row[1]

            pricepower = pricegarmkon + priceojrat + pricebox + pricepomp + pricevalve + pricemotor
        else:
            pricepower = 0

        print("++++++++++++++")
        if oil == 'on':
            oilprice = (int(pomp)+30)*22500
            pricepower = pricepower + (int(pomp)+30)*22500
        else:
            oilprice = 0

        if pipe == 'on':
            if pomp > 100 :
                pricepower = pricepower + 4*350000
            else : 
                pricepower = pricepower + 4*250000
        else:
            pipeprice = 0

        if handpomp == 'on':
            pricepower = pricepower + 1500000
        else:
            handpompprice = 0

        carslingprice = 0
        if carsling == 'on':
            if w2 >= 7 :
                carslingp = 12000000
            else : 
                carslingp = 9000000

            carslingprice = carslingprice + carslingp
        else:
            carslingprice = 0

        if tablo == 'on':
            carslingprice = carslingprice + 10000000

        total = carslingprice + pricepower + pricejack
        
        return {"totalp":total,
                "carp":carslingprice,
                "val":val,
                "rap":rap,
                "powerp":pricepower,
                "jackp":pricejack,
                "totalsode":place_value(int(total*(100+sode)/100)),
                "jacksode":place_value(int(pricejack*(100+sode)/100)),
                "powersode":place_value(int(pricepower*(100+sode)/100)),
                "carsode":place_value(int(carslingprice*(100+sode)/100)),
                "ourprofit": int(total*(100+sode)/100 - total) ,
                "info":js}, 200
    else : 
        return {},400

def calc(systemtype,v, w1 ,w2, l):
    
    if w1 == 0:
        if w2 >=2 and w2 <=4:
            if systemtype == "d":
                careslingweight = 250
            elif systemtype == "i":
                careslingweight = 300
        if w2 > 4 and w2 <= 6:
            if systemtype == "d":
                careslingweight = 400
            elif systemtype == "i":
                careslingweight = 500
        if w2 > 6 and w2 <= 8:
            if systemtype == "d":
                careslingweight = 600
            elif systemtype == "i":
                careslingweight = 700
        if w2 > 8 and w2 <= 10:
            if systemtype == "d":
                careslingweight = 650
            elif systemtype == "i":
                careslingweight = 800
        if w2 > 10 and w2 <= 12:
            if systemtype == "d":
                careslingweight = 750
            elif systemtype == "i":
                careslingweight = 1000

        w = int(w2)*75 + careslingweight
    else:
        w = w1
    if systemtype == "d":
        if l < 3:
            l = 3
        if l > 8 :
            flash("بهتر است از سیستم تلسکوپی استفاده کنید")
            return render_template('main.html')
            #js = {"info":"better to use Telescopice 2 or 3 stage"},400
            #return js
        g.cur.execute('SELECT * FROM shaft WHERE length >= ? and length < ?+1 and weight > ? ',(l,l,w),)
        for row in g.cur.fetchall():
            d = int(split(row[3], "-")[1])
            p = (w*400)/(d*d*pi)
            if p < 48 :
                dic = [row[0],row[1],p+5,row[3]]

                break
        print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
        print(dic)
        shaftL = dic[0]
        shaftW = dic[1]
        shaftB = dic[2]
        shaftT = dic[3]
        pomptypee = "s"
        state = False
        g.cur.execute('SELECT * FROM pomp WHERE pomptype = ? and shafttype = ? and v< ?+0.09 and v > ?-0.05',(pomptypee,shaftT, v, v),)
        for row in g.cur.fetchall():
            dic = [row[0],row[1],row[2],row[3],row[4]]
            state = True
            break
        pompdebi = dic[0]

        tavanmotor = (shaftB *pompdebi )/600
        if pompdebi > 80 and pompdebi < 101:
            if tavanmotor < 7.7 : 
                tavanmotor = 7.7
        if pompdebi > 100:
            if tavanmotor<9.5:
                tavanmotor = 9.5
        if tavanmotor <= 4.8:
            tavanmotor = 5.8
        elif tavanmotor >4.8 and tavanmotor <= 5.9 :
            tavanmotor = 5.8
        elif tavanmotor >5.9 and tavanmotor <= 7.8 :
            tavanmotor = 7.7
        elif tavanmotor >7.8 and tavanmotor <= 9.5 :
            tavanmotor = 9.5
        elif tavanmotor >9.5 and tavanmotor <= 11.1 :
            tavanmotor = 11
        elif tavanmotor >11.1 and tavanmotor <= 12.6 :
            tavanmotor = 12.5
        elif tavanmotor >12.5 and tavanmotor <= 14.8 :
            tavanmotor = 14.7
        elif tavanmotor >14.7 and tavanmotor <= 18.5 :
            tavanmotor = 18
        elif tavanmotor >22.1 and tavanmotor <= 24 :
            tavanmotor = 24

        if state is True : 
            js = {"shafttool":shaftL,"shaftmodel": shaftT ,"pomp": pompdebi, "v":v, "motor": tavanmotor},200
            return js
        else:
            flash("سرعت سیستم را بهتر است کاهش بدهید سیستمی منطبق با این مشخصات یافت نشد")
            return render_template('main.html')
            #js = {"info":"decrease youre V in direct system"},400
            #return js
    elif systemtype == "i":
        l = ceil(l/2)
        if l < 3:
            l = 3
        w = 2*w
        g.cur.execute('SELECT * FROM shaft WHERE length >= ? and length < ?+1 and weight > ? ',(l,l,w),)
        for row in g.cur.fetchall():
            d = int(split(row[3], "-")[1])
            p = (w*400)/(d*d*pi)
            if p < 45 :
                dic = [row[0],row[1],p+5,row[3]]
                break
        print(dic)
        shaftL = dic[0]
        shaftW = dic[1]
        shaftB = dic[2]
        shaftT = dic[3]

        pomptypee = "s"
        print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        print(dic)
        g.cur.execute('SELECT * FROM pomp WHERE pomptype = ? and shafttype = ? and v*2 < ?+0.09 and v*2 > ? -0.05',(pomptypee,shaftT, v, v),)
        for row in g.cur.fetchall():
            dic = [row[0],row[1],row[2],row[3],row[4]]
            break
        print(dic)
        pompdebi = dic[0]
        print(pompdebi)
        print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        tavanmotor = (shaftB *pompdebi )/600


        if pompdebi > 80 and pompdebi < 101:
            if tavanmotor < 7.7 : 
                tavanmotor = 7.7
        if pompdebi > 100:
            if tavanmotor<9.5:
                tavanmotor = 9.5

        if tavanmotor <= 4.8:
            tavanmotor = 5.8
        elif tavanmotor >4.8 and tavanmotor <= 5.9 :
            tavanmotor = 5.8
        elif tavanmotor >5.9 and tavanmotor <= 7.8 :
            tavanmotor = 7.7
        elif tavanmotor >7.8 and tavanmotor <= 9.5 :
            tavanmotor = 9.5
        elif tavanmotor >9.5 and tavanmotor <= 11.1 :
            tavanmotor = 11
        elif tavanmotor >11.1 and tavanmotor <= 12.6 :
            tavanmotor = 12.5
        elif tavanmotor >12.5 and tavanmotor <= 14.8 :
            tavanmotor = 14.7
        elif tavanmotor >14.7 and tavanmotor <= 18.5 :
            tavanmotor = 18.5
        elif tavanmotor >22.1 and tavanmotor <= 24 :
            tavanmotor = 24
        
        js = {"shafttool":shaftL,"shaftmodel": shaftT ,"pomp": pompdebi, "v":v, "motor": tavanmotor},200
        return js

    elif systemtype == "t":
        return None
    elif systemtype == "b":
        return None
    else:
        return False


def factor_price(dic):
    print(dic)
    customer_name = dic['customer_name']
    project_name = dic['project_name']
    phone_number= dic['phone_number']
    address= dic['address']
    systemtype = dic['systemtype']
    travel= float(dic['travel'])
    w1= int(dic['w1'])
    w2= int(dic['w2'])
    v= float(dic['v'])
    sode= int(dic['sode'])
    pishprice= dic['pishprice']
    jack= dic['jack']
    power= dic['power']
    carsling= dic['carsling']
    rapcher= dic['rapcher']
    oil= dic['oil']
    pipe= dic['pipe']
    handpomp= dic['handpomp']
    tablo= dic['tablo']
    dic2 = calprice(systemtype,travel,w1,w2,v,power,oil,pipe,handpomp, jack , rapcher, carsling ,tablo , sode)

    ###### TODO Save to date Base

    print("ddiasdjiasidaisjdiajsdijasijdiasjd-------")

    for item in dic2[0]['info'][0]:
        dic['{}'.format(item)]=dic2[0]['info'][0]['{}'.format(item)]
    for item in dic2[0]:
        dic['{}'.format(item)] = dic2[0]['{}'.format(item)]

    if systemtype == 'i':
        dic['systemtype'] = "غیر مستقیم"
    elif systemtype == 'd':
        dic['systemtype'] = "مستقیم"

    shanb,month,day,year,saat = today1()
    date = day+" "+month+" "+year
    dic['date'] = date

    ### TODO insert DATA to DB
    print("start save in DB")
    g.cur.execute('INSERT INTO factors(customer_name,project_name,address,systemtype,travel,w1,w2,jack,power,carsling,oil,pipe,rapcher,handpomp,shafttool,shaftmodel,pomp,motor,date,val,totalsode,jacksode,powersode,carsode,ourprofit) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',(dic['customer_name'],dic['project_name'],dic['address'],dic['systemtype'],dic['travel'],dic['w1'],dic['w2'],dic['jack'],dic['power'],dic['carsling'],dic['oil'],dic['pipe'],dic['rapcher'],dic['handpomp'],dic['shafttool'],dic['shaftmodel'],dic['pomp'],dic['motor'],dic['date'],dic['val'],dic['totalsode'],dic['jacksode'],dic['powersode'],dic['carsode'],dic['ourprofit']),)
    g.db.commit()
    print("finish save in DB")
    return dic, dic2



@app.before_request
def befor_request_hook():
    g.db = create_db()
    g.cur = g.db.cursor()

    
@app.after_request
def after_request_hok(response):
    g.db.close()
    return response


# handle login failed
@app.errorhandler(401)
def page_not_found(e):
    return Response('<p>Login failed</p>')
    
    
# callback to reload the user object        
@login_manager.user_loader
def load_user(userid):
    return User(userid)

@app.route("/logout/")
@login_required
def logout():
    logout_user()
    return redirect("/dashboard")

@app.route("/login/", methods=["GET", "POST"])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']        
        if password == config.password and username == config.username:
            user = User(0)
            login_user(user)
            return redirect("/dashboard")
            return redirect(request.args.get("next"))
        else:
            flash("شماره تلفن یا رمز عبور اشتباه وارد شده است")
            return render_template('accounts/auth-login.html')
    else:
        return render_template('accounts/auth-login.html')


@app.route('/', methods=['GET', 'POST'])
def main():
    return render_template('landing/index.html')

@app.route('/soon/', methods=['GET', 'POST'])
def soon():
    return render_template('page-coming-soon.html')

@app.route('/factors/', methods=['GET', 'POST'])
def factors():
    listmoshtari = []
    g.cur.execute('SELECT * FROM factors')
    for row in g.cur.fetchall():
        w1 = int(row[5])
        w2 = int(row[6])
        if w1 == 0 :
            weight = w2
        else:
            weight = w1
        dic = {"customer_name":row[0] , 
                "project_name":row[1] ,
                "systemtype": row[3],
                "travel":row[4] ,
                "weight":weight ,
                "price":row[20] }
        listmoshtari.append(dic)
    if request.args.get('factor'):
        customer_name = request.args.get('customer_name')
        project_name = request.args.get('project_name')
        travel = request.args.get('travel')
        total = request.args.get('total')
        g.cur.execute('SELECT * FROM factors WHERE customer_name == ? and project_name == ? and travel == ? and totalsode == ?',(customer_name,project_name,travel,total),)
        for row in g.cur.fetchall():
            dic = {"customer_name":row[0] , 
                    "project_name":row[1] ,
                    "address":row[2],
                    "systemtype": row[3],
                    "travel":row[4] ,
                    "systemtype":row[5],
                    "w1":row[6],
                    "w2":row[7],
                    "jack":row[8],
                    "power":row[9],
                    "carsling":row[10],
                    "pipe":row[11],
                    "rapcher":row[12],
                    "handpomp":row[13],
                    "shafttool":row[14],
                    "shaftmodel":row[15],
                    "pomp":row[16],
                    "motor":row[17],
                    "date":row[18],
                    "val":row[19],
                    "totalsode":row[20],
                    "jacksode":row[21],
                    "powersode":row[22],
                    "carsode":row[23],
                    "ourprofit":row[24]}
        return render_template('page-invoice.html',dic1= dic,dic2= dic)

    
    elif request.args.get('remove'):
        customer_name = request.args.get('customer_name')
        project_name = request.args.get('project_name')
        travel = request.args.get('travel')
        total = request.args.get('total')

        g.cur.execute('DELETE FROM factors WHERE customer_name == ? and project_name == ? and travel == ? and totalsode == ?',(customer_name,project_name,travel,total),)
        g.db.commit()
        return redirect('/factors')
    else:
        return render_template('factors.html', list_m =listmoshtari )

@app.route('/dashboard/', methods=['GET', 'POST'])
@login_required
def panel():

    if request.method == 'POST':
        travel = request.form['travel']
        weight = request.form['weight']
        #try :
        travel = float(travel)
        weight = int(weight)
        phone_number = request.form['phone_number']
        customer_name = request.form['customer_name']
        project_name = request.form['project_name']
        if (project_name and customer_name and travel and weight) is not None :
            if  travel < 30:
                if weight <20 :
                    w1 = 0
                    w2 = weight
                else:
                    w1 = weight
                    w2 = 0
                dictionary = {
                    'w1':w1,
                    'w2':w2,
                    'jack':'off',
                    'power' : 'off',
                    'carsling' : 'off',
                    'rapcher' : 'off',
                    'oil' : 'off',
                    'pipe' : 'off',
                    'handpomp' : 'off',
                    'tablo' : 'off',
                    'pishprice' : 'off'}

                a = request.form
                for x in a:
                    dictionary['{}'.format(x)]=a['{}'.format(x)]
                print("*********")
                dic11, dic22 = factor_price(dictionary)
                

                return render_template('page-invoice.html',dic1= dic11,dic2= dic22)

            else:
                flash("مقدار جابه جایی نمیتواند بیشتر از 30 متر باشد")
                return render_template('main.html')
        else:
            flash("پر کردن برخی فیلد ها اجباری میباشد")
            return render_template('main.html')
        #except:
            #flash(" مقدار جا به جایی آسانسور یا وزن آن دچار مشکل است لطفا به صورت صحیح وارد کنید")
            #return render_template('main.html')
    else:
        sode = 0
        g.cur.execute('SELECT * FROM factors')
        for row in g.cur.fetchall():
            value = int(row[24])
            sode = sode + value

        dic = {'sode':place_value(sode)}
        return render_template('main.html',item = dic)


@app.route('/test/', methods=['GET', 'POST'])
@login_required
def map():


    if request.args.get('factor'):
        customer_name = request.args.get('customer_name')
        project_name = request.args.get('project_name')
        travel = request.args.get('travel')
        total = request.args.get('total')
        g.cur.execute('SELECT * FROM factors WHERE customer_name == ? and project_name == ? and travel == ? and totalsode == ?',(customer_name,project_name,travel,total),)
        for row in g.cur.fetchall():
            dic = {"customer_name":row[0] , 
                    "project_name":row[1] ,
                    "address":row[2],
                    "systemtype": row[3],
                    "travel":row[4] ,
                    "systemtype":row[5],
                    "w1":row[6],
                    "w2":row[7],
                    "jack":row[8],
                    "power":row[9],
                    "carsling":row[10],
                    "pipe":row[11],
                    "rapcher":row[12],
                    "handpomp":row[13],
                    "shafttool":row[14],
                    "shaftmodel":row[15],
                    "pomp":row[16],
                    "motor":row[17],
                    "date":row[18],
                    "val":row[19],
                    "totalsode":row[20],
                    "jacksode":row[21],
                    "powersode":row[22],
                    "carsode":row[23],
                    "ourprofit":row[24]}
        return render_template('page-invoice.html',dic1= dic,dic2= dic)

    
    elif request.args.get('remove'):
        customer_name = request.args.get('customer_name')
        project_name = request.args.get('project_name')
        travel = request.args.get('travel')
        total = request.args.get('total')

        g.cur.execute('DELETE FROM factors WHERE customer_name == ? and project_name == ? and travel == ? and totalsode == ?',(customer_name,project_name,travel,total),)
        g.db.commit()
        return redirect('/factors')

    #g.cur.execute('INSERT INTO test(test1,test2,test3,test4) VALUES (?,?,?,?)',(dic['customer_name'],dic['project_name'],dic['address'],dic['systemtype']),)
    #g.db.commit()
    else:
        return 'a'


@app.route('/wiz/', methods=['GET', 'POST'])
@login_required
def user():
    return render_template('table-datatable.html')

@app.route('/api/v1/cal/<string:auth>',methods=['POST'])
def websiteapi(auth):
    if auth == 'F3E49523300186B1BF692BBE5532B5AD9F31C03F6CB47486EF5B97E20B24F764':
        args = request.get_json()
        if args is not None:
            l = float(args['l'])
            w1 = int(args['w1'])
            w2 = int(args['w2'])
            v = float(args['v'])
            s = str(args['s'])
            return jsonify(calc(s,v,w1,w2,l))
        else:
            return {},400

@app.route('/api/v1/pricing/<string:auth>',methods=['POST'])
def websiteapiprice(auth):
    if auth == 'F3E49523300186B1BF692BBE5532B5AD9F31C03F6CB47486EF5B97E20B24F764':
        args = request.get_json()
        
        if args is not None:
            l = float(args['l'])
            w1 = int(args['w1'])
            w2 = int(args['w2'])
            v = float(args['v'])
            s = str(args['s'])
            power = args['power']
            oil = args['oil']
            pipe = args['pipe']
            handpomp = args['handpomp']
            jack = args['jack']
            rapcher = args['rapcher']
            carsling = args['carsling']
            boxel = args['boxel']
            boxelmetr = args['boxelmetr']

            return jsonify(calprice(s,l,w1,w2,v,power, oil,pipe,handpomp, jack, rapcher,carsling, boxel, boxelmetr))

        else:
            return {},400


if __name__ == '__main__':
    app.run('0.0.0.0',5000,debug=1)